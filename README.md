# Tutorial de programación de ARM Cortex-M con herramientas libres

Para este tutorial estoy utilizando:

* **HARDWARE:** Blue Pill (STM32F103C8) y ST-Link/V2
* **SOFTWARE:** PlatformIO en VS Code con libOpenCM3 sobre Debian GNU/Linux.

## Ejemplo 02: Basic GPIOs

Este ejemplo está apuntado a la utilización básica de Entradas/Salidas.

Entrada del blog: [Adentrandonos en las entrañas del STM32F1](https://electronlinux.wordpress.com/2020/06/05/adentrandonos-en-las-entranas-del-stm32f1/)

Video en YouTube: [Configuración del SYSCLK y utilización de GPIOs con PlatformIO y libOpenCM3](https://youtu.be/2QIvTn5HRPc)
