/**
 * @file main.c
 * @author Matías S. Ávalos (@tute_avalos)
 * @brief Basic-GPIOs con libOpenCM3
 * @version 0.1
 * @date 2020-06-05
 * 
 * @copyright Copyright (c) 2020
 * 
 * MIT License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

int main()
{
    // SYSCLK a 72Mhz
    rcc_clock_setup_in_hse_8mhz_out_72mhz();

    // Se habilitan los clocks de los puertos a utilizar:
    rcc_periph_clock_enable(RCC_GPIOC);
    rcc_periph_clock_enable(RCC_GPIOB);

    // Configuramos el LED de la BluePill PC13
    gpio_set_mode(
        GPIOC,
        GPIO_MODE_OUTPUT_2_MHZ,
        GPIO_CNF_OUTPUT_PUSHPULL,
        GPIO13);

    gpio_set(GPIOC,GPIO13);     // LED1 apagado

    // Configuramos los LED2 y LED3 (PB5 y PB6)
    gpio_set_mode(
        GPIOB,
        GPIO_MODE_OUTPUT_2_MHZ,
        GPIO_CNF_OUTPUT_PUSHPULL,
        GPIO5 | GPIO6);

    gpio_set(GPIOB, GPIO5);     // LED2 prendido
    gpio_clear(GPIOB, GPIO6);   // LED3 apagado

    // Configuramos el pulsador PB15
    gpio_set_mode(
        GPIOB,
        GPIO_MODE_INPUT,
        GPIO_CNF_INPUT_FLOAT,   // tiene pull-up externo
        GPIO15);                

    while (1)
    {
        if (gpio_get(GPIOB, GPIO15) == 0)   // Si el pulsador está presionado
            gpio_toggle(GPIOB, GPIO6);      //   invertimos el LED

        // Siempre se invierte el LED2
        gpio_toggle(GPIOB, GPIO5);  

        // El PC13 está prendido cuando el PB5 está prendido
        gpio_toggle(GPIOC, GPIO13); 

        for (uint32_t i = 0; i < 7200000; ++i)
            __asm__("nop");
    }
}